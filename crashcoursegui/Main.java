import javax.swing.JFrame;

import controller.GameController;
import model.GameModel;
import view.GameView;

public class Main {

    public static void main(String[] args) {
        GameModel model = new GameModel();
        GameView view = new GameView(model);
        new GameController(model, view);

        
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(view);
        frame.pack();
        frame.setVisible(true);
    }
}