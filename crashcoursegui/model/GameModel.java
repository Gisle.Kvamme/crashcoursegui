package model;

import java.awt.Color;

import controller.ControllableModel;
import model.grid.CellPosition;
import model.grid.Grid;
import model.grid.GridCell;
import model.grid.GridDimension;
import view.ViewableModel;

public class GameModel implements ViewableModel, ControllableModel {
    private Grid<Color> grid;
    private ColorGenerator colorGenerator;

    public GameModel() {
        this.grid = new Grid<>(9,9);
        this.colorGenerator = new ColorGenerator();
    }

    @Override
    public Color get(CellPosition pos) {
        return grid.get(pos);
    }

    @Override
    public GridDimension getDimension() {
        return grid;
    }

    @Override
    public Iterable<GridCell<Color>> getIterable() {
        return grid;
    }

    @Override
    public void set(CellPosition pos) {
        grid.set(pos, colorGenerator.getRandom());
    }

    
}
