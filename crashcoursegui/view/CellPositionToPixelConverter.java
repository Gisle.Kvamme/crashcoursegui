package view;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import model.grid.CellPosition;
import model.grid.GridDimension;

public class CellPositionToPixelConverter {

  private final Rectangle2D box;
  private final GridDimension gd;
  private final double margin;
  private double cellW;
  private double cellH;


  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
    this.cellW = (box.getWidth() - margin * gd.cols() - margin) / gd.cols();
    this.cellH = (box.getHeight() - margin * gd.rows() - margin) / gd.rows();
  }

  public Rectangle2D getBoundsForCell(CellPosition cellPosition) {
    double cellW = (box.getWidth() - margin * gd.cols() - margin) / gd.cols();
    double cellH = (box.getHeight() - margin * gd.rows() - margin) / gd.rows();
    double cellX = box.getX() + margin + (cellW + margin) * cellPosition.col();
    double cellY = box.getY() + margin + (cellH + margin) * cellPosition.row();
    return new Rectangle2D.Double(cellX, cellY, cellW, cellH);
  }


  public CellPosition getCellPositionOfPoint(Point2D point) {
    // Same math as getBoundsForCell, but isolate the col/row on one side
    // and replace cellX with point.getX() (cellY with point.getY())
    double col = (point.getX() - box.getX() - margin) / (cellW + margin);
    double row = (point.getY() - box.getY() - margin) / (cellH + margin);

    return new CellPosition((int) row, (int) col);
}
}